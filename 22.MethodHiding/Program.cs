﻿using System;




//   *******************************************
//    Part 22  : Method Hiding  - START
/* 
 *            - Method Hiding
 *            - Invoke Hidden base class members
 *            
 *            
 *            - If you want to hide base class member use the new keyword
 *              example in 2nd method. ( public new void PrintFullName())
 *              
 *            - Use new keyword to hide a base class member. You will get a compiler warrnirn if you miss the new keyword
 *            
 *            1. Use Base keyword
 *            2. Cast Child type to parent type and invoke the hidden member
 *            3. ParentClass PC = new ChildClass() PC.HiddenMethod()
 *            
 *            
*/


namespace _22.MethodHiding
{


    public class Employee
    {
        public string FirstName;
        public string  LastName;

        //1st method
        public void PrintFullName()
        {
            Console.WriteLine($"{FirstName} {LastName}");
        }

        
    }

    public class PartTimeEmployee : Employee
    {
        //2nd method.
        //if we put 2nd method here it will hide 1st method parent class method
        //it says that it hides the inherited method if its intended, if use new keyword
        public new void PrintFullName()  
// we can also use base >   base.PrintFullName()
        {
            Console.WriteLine($"{FirstName} {LastName} - Contractor");
        }
    }

    public class FullTimeEmployee : Employee
    {

    }


    
    class Program
    {
        static void Main()
        {
            FullTimeEmployee FTE = new FullTimeEmployee();
            FTE.FirstName = "Mark";
            FTE.LastName = "Whalberg";
            FTE.PrintFullName();

            PartTimeEmployee PTE = new PartTimeEmployee();
            PTE.FirstName = "Ivo";
            PTE.LastName = "Ivic";
            PTE.PrintFullName();
        }
    }
}



//   *******************************************
//    Part 22  : Method Hiding  - END