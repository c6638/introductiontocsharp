﻿using System;


//   *******************************************
//    Part 10  : IF Statement  - START
/*                  - it is conditional statement
 *                  - we have 2 types of conditional statements in C#
 *                  - if and switch
 *                  
 *                  
 *                  -If statem
 *            
*/



/* We want this program to: 
 *  - present IF statement
 *  - present IF ELSE statemenet
 *  - present difference between && and &
 *  - present difference between || and | 
 */


namespace _10.IfStatement
{

    internal class Program
    {


        public static void Main()
        {
            //Parse1();
            //Parse2();
            //Parse3();
            //TryPars1();
            //TryParsCheckIfNumber();
            TryParseFullCheck();
        }


        // I want user to write a number, and I want to decide is it 1,2 or 3, using simple IF statement
        // when user enters number we have to assign it to integer variable

        static void Parse1()
        {

            Console.WriteLine("Please enter a number:");
            // int UserNumber = Console.ReadLine();  // we get compile error at first because readline returns string, not number
            //string cannot be implicitly converted. we have to convert string format to integer format. (using parse here)

            int UserNumber = int.Parse(Console.ReadLine());  // now we will have user number and we can compare



            if (UserNumber == 1)
            {
                Console.WriteLine($"Your number is: One");
            }
            else if (UserNumber == 2)
            {
                Console.WriteLine($"Your number is: Two");
            }
            else if (UserNumber == 3)
            {
                Console.WriteLine($"Your number is: Three");
            }
            else
            {
                Console.WriteLine("Your number is not between 1 and 3");
            }


        }

        static void Parse2()
        {
            Console.WriteLine("Please enter a number:");

            int UserNumber = int.Parse(Console.ReadLine());

            if (UserNumber == 10 || UserNumber == 20)    // | pipe checks every condition but ,|| or double pipe -> if any of conditions is executed,if its double pipe if 1st conditions is true it doesnt bother with next conditions. Same for && .
            {
                Console.WriteLine("Your number is 10 or 20");
            }
            else
            {
                Console.WriteLine();
            }
        }


        //********************
        //Iz Stringa u Number:

        static void Parse3()
        {

            string myString = "5";   //"a5"  -> ce dat exception

            int myNumber = int.Parse(myString);

            Console.WriteLine($"Parsom dobiveno iz stringa u number = {myNumber}");

        }




        //********************
        // Sa tryParsom - koji i daje bool true/false

        static void TryPars1()
        {

            string myString = "a5";   //"a5"  -> ce dat false

            bool a = int.TryParse(myString, out int myNumber);

            Console.WriteLine($"TryParsom dobiveno = {myString} and bool is {a},  {myNumber}");

        }
    
        // tryPare > if number
    static void TryParsCheckIfNumber()
    {
        Console.WriteLine("Please enter a number : ");
        var userNumber = Console.ReadLine();

            bool a;

            if(a = int.TryParse(userNumber, out var userSomething))
            {
                Console.WriteLine($"You entered a number :{userSomething}");
            }
            else
            {
                Console.WriteLine($"You didn't enter a number : {userNumber}");
            }
            Console.WriteLine($"You entered : {userNumber}");

    }



        // tryPare > fullCheck if number :
        static void TryParseFullCheck()
        {
            Console.WriteLine("Please enter a number  between 1 - 20 ");

            var userNumber = Console.ReadLine();

            int userSomething;

            bool a;

            if (a = int.TryParse(userNumber, out userSomething))
            {
                // Console.Writeline($"You entere a number : {userNumber}");

                if(userSomething == 10)
                {
                    Console.WriteLine($"Your number is 10!");
                }
                else if(userSomething >= 0 && userSomething <=10)
                {
                    Console.WriteLine($"Your number is between 0 and 10! to be specific : {userSomething}");
                }
                else if (userSomething >=10 && userSomething <=20)
                {
                    Console.WriteLine($"Your number is between 10 and 20! to be specific: {userSomething}");
                }
            }
            else
            {
                Console.WriteLine($"You didn't enter a number! :{userNumber}");
            }
            Console.WriteLine($"You entered : {userNumber}");
            
             
        }



    }

}





//   *******************************************
//   Part 10 : IF Statement  - END