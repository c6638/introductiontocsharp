﻿using System;


//   *******************************************
//    Part 23  : Polymorphism  - START
/* 
 *            - Polymorphism enables you to invoke derived class methods through base class refernence variable at runtime!
 *            
 *           
 *           - Polymorhphism is one of the primary pillars of object-oriented programming.
 *           
 *           - Polymorphism allows you to invoke derived class methods through a base class reference during runtime.
 *           
 *           - In the base class the method is declared virtual, and in the derived class we override the same method.
 *           
 *           - The virtual keyword indicated, the method can be overriden in any derived class
 *            
*/


namespace _23.Polymorphism
{

    public class Employee
    {
        public string FirstName = "MyFirstName";
        public string LastName = "MyLastName";


        //all of these classes inherit from base class:
        //and now we want to seperate and knonw if its parttime/fulltime
       
        //  public void PrintFullName()  1.before
        public virtual void PrintFullName() //we will mark public void virtual in your parent class,
                                            // the moment we mark method as virtual, it basically indicates to derived class!
                                            // any derived class can override the method, if derived class wishes to do so.
        {
            Console.WriteLine($"{FirstName} {LastName}");
        }
    }

    public class PartTimeEmployee : Employee
    {

        //1.before
        //public virtual void PrintFullName() 
        //{
        //    Console.WriteLine($"{FirstName} {LastName} - Part Time ");
        //}

        //2.after
        public override void PrintFullName()
        {
            Console.WriteLine($"{FirstName} {LastName} - Part Time ");
        }


    }

    public class FullTimeEmployee : Employee
    {

        //if hide is intentional then use "new" keyword
        public override void PrintFullName() //if we do it like this 
                                            //if the method in the child class has the same name,signature it will hide parent one
                                            //and there will be no difference in the output
                                    
        {
            Console.WriteLine($"{FirstName} {LastName} - Full Time ");
        }


    }

    public class TemporaryTimeEmployee : Employee
    {

        //1. before public void PrintFullName()
        //2. here we have overriden method as well: 
        public override void PrintFullName()
        {
            Console.WriteLine($"{FirstName} {LastName} - Temp - Time ");
        }



    }

    internal class Program
    {
        static void Main()
        {

            Employee[] employees = new Employee[4];

            employees[0] = new Employee();
            employees[1] = new PartTimeEmployee();  //base class reference variable can point to child class object
            employees[2] = new FullTimeEmployee();
            employees[3] = new TemporaryTimeEmployee();

            //2.After doing virtual on base, override on rest methods:
            // now we will get for each class FirstName LastName and + rest
            foreach (Employee e in employees)
            {
                e.PrintFullName();
            }

        }
    }
}






//   *******************************************
//    Part 23  : Polymorphism  - END