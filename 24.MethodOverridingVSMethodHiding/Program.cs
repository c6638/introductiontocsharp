﻿using System;


//   *******************************************
//    Part 24  : Method Overriding VS Method Hiding  - START
/* 
 *            - 
 *            
*/



namespace _24.MethodOverridingVSMethodHiding
{



    public class BaseClass1
    {

        public virtual void Print()
        {
            Console.WriteLine($"I'm a Base Class Print Method!");
        }
    }

    //1. Method Method Overriding
    public class DerivedClass1 : BaseClass1
    {

        //overriding base class in child class
        public override void Print()
        {
            Console.WriteLine($"I'm a Child Class Print Method!");
        }

    }
    //2. Method Hiding
    public class DerivedClass2 : BaseClass1
    {

        //overriding base class in child class
        public new void Print()
        {
            Console.WriteLine($"I'm a Child Class Print Method!");
        }

    }



    internal class Program
    {
        static void Main()
        {
            //1. Method Overriding
            BaseClass1 B = new DerivedClass1();
            B.Print();

            //2. Method Hiding
            BaseClass1 D = new DerivedClass2();
            D.Print();  

        }
    }



    //2.method Hiding
}



//   *******************************************
//    Part 24  : Method Overriding VS Method Hiding  - END


