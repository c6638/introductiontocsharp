﻿using System;

//   *******************************************
//    Part 8  : Arrays  - START
/*            
 *            An array is a collection of similar data types.
 *            Examples:
 *            
 *            - initialize and Assign Values in different lines
 *                      int[] EvenNumbers = new int [3];
 *                      EvenNumbers[0] = 0;
 *                      EvenNumbers[1] = 2;
 *                      EvenNumbers[2] = 4;
 *                      
 *           - initialize and Assign values in the same line
 *                      int[] OddNumbers = {1,2,3};
 *                      
 *     Advantages: Arrays are strongly typed.
 *     
 *     Disadvantages: Arrays cannot grow in size once you define the size and its initialized.
 *     Have to rely on integral indices to store or retrieve items from the array.
 *        
 *     The only way to store retrieve elements from arrays is to use integral indices(indexi)
 *            
 *            
 *            
*/



/* We want this program to: 
 * 
 */

namespace _8.Arrays
{


    internal class Program
    {

        static void Main()
        {
            int a = 10;

            // if u want to store a group of similar dataypes in a single variable
            // lets say collection of even numbers - then we use array of integer numbers
             
            int[] EvenNumbers = new int[3]; // new -int its a group of integers, and how many do we want to store, we specify the size new int[3]
           
            //arrays are integer index based,starting from 0
            EvenNumbers[0] = 0;   // array is strongly typed, so since its integer arrays we cannot put it as string 
            EvenNumbers[1] = 2;
            EvenNumbers[2] = 4;
            EvenNumbers[3] = 6;  // if we try to compile this, but if we run this we will get exception, index out of range.
                                 // It means array size is 3 and we are trying to store 4th element into array which is not possible.
                                 // arrays cannot grow in size (it's an dissadvantage, but we have other collection classes- >

            Console.WriteLine(EvenNumbers[1]);



            // the only way to store retrieve elements from arrays is to use integral indices(indexi)


        }
    }
}






//   *******************************************
//   Part 8 : Arrays - END