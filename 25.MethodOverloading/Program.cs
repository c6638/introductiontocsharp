﻿using System;

//   *******************************************
//    Part 25  : Method Overloading  - START
/* 
 *            - Function overloading and Method overloading terms are used interchangably
 *            
 *            - Method overloading allows a class to have multiple methods with the same name, but, with a different signature.
 *            - So in C# functions can be overloaded based on the number, type(int,float etc) and kind (value,ref or out) of parameters.
 *            
 *            - The signature of a method does not include the return type and the params modifier. So it is not possible to overload a function,
 *              just based on the return type of params modifier.
 *            
*/


namespace _25.MethodOverloading
{
    internal class Program
    {
        static void Main()
        {   // while writing method, we can choose which we will use 1,2,3  (overloaded based on number, and parameters)
            int myFN1 = 1;
            int myFN2 = 2;
            

            Adding(4,6) ;
        }


        //we can overload a function based on number of parameters:

        public static void Adding(int FN, int SN)
        {
            Console.WriteLine($"Sum = {FN+SN}");
        }

        public static void Adding(int FN, int SN, int TN)
        {
            Console.WriteLine($"Sum = {FN + SN}");
        }

        public static void Adding(int FN, int SN, int TN, int Third)
        {
            Console.WriteLine($"Sum = {FN + SN}");
        }

        public static void Adding(int FN, int SN, out int Sum)
        {
            Console.WriteLine($"Sum = {FN + SN}");
            Sum = FN + SN;
        }



    }
}



//   *******************************************
//    Part 25  : Method Overloading  - END


