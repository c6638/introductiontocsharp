﻿using System;




//   *******************************************
//    Part 18  : Namespaces  - START
/* 
 *              - Namespaces basics
 *              - Using alias directive
 *              - Different namespace members
 *              - Namespace can contain
 *                  1.Another namsepace
 *                  2.Class
 *                  3.Interface
 *                  4.Struct
 *                  5.enum
 *                  6.delegate
 *              
 *              - Namespaces are used to organize your programs.
 *              - They also provide assistance in avoiding name clashes.
 *              
 *              - Namespaces don't correspoond to file, dierctory or assembly names.They could be written in seperate file and still belong to same namespace
*/



/* We want this program to: 
 *  
 *  Project A, Project B
 *  Team A, Team B
 *  Both teams work on project A
 *  Identify Project A with TeamA,TeamB
 */

namespace _18.Namespaces
{

     class Program
    {

       public static void Main()
        {

            //if we want to use projectA and class from teamA we write like this, or we using Using-then namespace
                    ProjectA.TeamA.ClassA.Printing();
                    ProjectA.TeamB.ClassA.Printing();

            

        }


    }

}

namespace ProjectA
{

    namespace TeamA
    {
        class ClassA
        {
            public static void Printing()
            {
                Console.WriteLine("Team A - print method");
            }
        }
    }

}


namespace ProjectA
{

    namespace TeamB
    {
        class ClassA
        {
            public static void Printing()
            {
                Console.WriteLine("Team B - print method");
            }
        }
    }

}




//   *******************************************
//   Part 18  : Namespaces  - END


