﻿using System;


//   *******************************************
//    Part 6 : Nullable Types  - START
/*             In C# types are divided into 2 broad categories
 *             - Value Types : int, float, double, structs, enums etc
 *             - Reference Types : Interface, Class, delegates, arrays, etc
 *             
 *      By default value types are non nullable.
 *      int i = 0   - i is non nullabel so cannot be set to null, will generate comiler error
 *      int? j = 0   - j is nullable int, so null is legal
 *             
 *             - How are we going to store the value if the user doesnt want to fill with value yes/no option:
 *                we use null, to make value nullable we mark "?" next to the type  (  int? j = 0  )
*/



/* We want this program to: 
 
 */


namespace _6.NullableTypes
{

    internal class Program
    {

        static void Main()
        {

            string Name = null;   // reference types can have null, no error

    //        int i = null;        // compile error. Value types cannot be null like that.

            int? j = null;      // We made it nullable so now Value type is with null value.

            bool? areYouMajor = null;   // we made bool null - if user doesnt fill it.
        }




        /* We want this program to: 
            
            - print avaliable tickets
            - lets say tickets on sale are not avaliable, are null

        */
        static void TicketSale()
        {

            int? TicketsOnSale = null;   

            int AvaliableTickets;

            if(TicketsOnSale == null)
            {
                AvaliableTickets = 0;   // if it is null then its 0. Non nullable int type.
            }
            else
            {
                //  AvaliableTickets = TicketsOnSale;   we have an error, beacause we cannot implicitly convert nonvalualbe type and nullable type
                //  AvaliableTickets = TicketsOnSale.Value;  // this is one way to do it with value.
                   AvaliableTickets = (int)TicketsOnSale;   // we are forcing nullable data type to be converted to non nullable data type
            }

            Console.WriteLine($"Avaliable tickets : {AvaliableTickets}");


        }


        // Null Coalescing Operator ??
        // We can use Null Coalesce operator ??


        static void TicketSale2()
        {

            int? TicketsOnSale = null;

            // if tickets on sale is null, use this default value, otherwise use value that is present in this variable

            int AvaliableTickets = TicketsOnSale ?? 0;



            Console.WriteLine($"Avaliable tickets : {AvaliableTickets}");


        }
    }
}








//   *******************************************
//   Part 6 : Nullable Types - END