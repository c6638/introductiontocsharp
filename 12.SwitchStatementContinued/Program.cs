﻿using System;


//   *******************************************
//    Part 12  : Switch Statement Continued  - START
/* 
 *       - switch statement
 *       - break statement - is used inside a switch statement the controll will leave the switch statement
 *       - goto statement  - you can either jump to another case statement or to a specific label  ((it is a bad practice to use goto))
 *     
 *     - Multiple if statements can be replaced with a switch statement
*/



/* We want this program to: 
 *  - to enable to choose and buy coffee
 *  - then present the number of coofee's bought
 * 
 */


namespace _12.SwitchStatementContinued
{
    internal class Program
    {

        static void Main()
        {
        Start:
            int TotalCoffeeCost = 0;
            Console.WriteLine("Welcome to coffee shop! Please select your coffee size: \n1-Small \n2-Medium \n3-Large");
            int userChoice = int.Parse(Console.ReadLine());

            switch (userChoice)
            {
                case 1:
                    TotalCoffeeCost += 1;
                    break;
                case 2:
                    TotalCoffeeCost += 2;
                    break;
                case 3:
                    TotalCoffeeCost += 3;
                    break;
                default:
                    Console.WriteLine($"Your choice {userChoice} is invalid");
                    goto Start;

            }

        Mid:
            Console.WriteLine("Do you want to buy another coffee ? Type yes or no");
            string userDecision = Console.ReadLine();
            switch (userDecision)
            {

                case "yes":
                    goto Start;
                case "no":
                    break;

                default:
                    Console.WriteLine("Your answer is invalid, it should be either yes or no");
                    goto Mid;

            }

       // End:
            Console.WriteLine("Thank you for shopping with us!");
            Console.WriteLine($"Total coffee bought: {TotalCoffeeCost}");
       
           
        }
    }
}



//   *******************************************
//   Part 12  : Switch Statement Continued  - END
