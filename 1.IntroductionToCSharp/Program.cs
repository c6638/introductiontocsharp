﻿using System;

// - usingSystem- indicates that you are using the System namespace
// - Namespace is used to organize your code and is collection of classes,interfaces,structs,enums and delegates.
// - If we remove System namespace, Console.Writeline() wouldn't work, exist.


//   *******************************************
//    Part 1 : Introduction - START
/*              - Basic structure of C# program
 *              - What is a Namespace
 *              - Purpose of Main method
 * 
 *   -test12345
 */


// - We want this program to print very simple message to console.

namespace _1.IntroductionToCSharp
{

  class Program
    {
        // will this message get printed? (yes if we call it in main method)
        static void Main1()
        {
            Console.WriteLine($"Hello2! \nWelcome to C# Training2!");
        }


   //* Main method is the entry to your application

        static void Main() 
        {
            Console.WriteLine($"Hello! \nWelcome to C# Training!");
            Main1(); // calling Main1 method
        }
    }

}


//   *******************************************
//    Part 1 : Introduction - END

