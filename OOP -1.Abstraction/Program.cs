﻿using System;


//   *******************************************
//    Part   : OOP - 1.Abstraction  - START
/*            
 *                     - Abstraction is nothing but hiding an implementation
 *            
 *            
 *     Abstraction Modeling the relevant attributes and interactions of entities as classes to define an abstract representation of a system.
Encapsulation Hiding the internal state and functionality of an object and only allowing access through a public set of functions.
Inheritance Ability to create new abstractions based on existing abstractions.
Polymorphism Ability to implement inherited properties or methods in different ways across multiple abstractions.       
 *            
 *            
 *            
 *            
 *            
*/



/* We want this program to: 
 * 
 */


namespace OOP__1.Abstraction
{
    //abstract class, and all of members of the class should have abstract as well
    abstract class AditionClass
    {
        abstract public 

    }
    class Program
    {
        static void Main()
        {
        
        }
    }
}




//   *******************************************
//   Part  : OOP -1.Abstraction - END

