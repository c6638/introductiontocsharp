﻿using System;



//   *******************************************
//    Part 27  : Properties  - START
/*           
 *           - read / write
 *           - read only
 *           - write only
 *           - auto implemented properties
 *            
 *            In C# to encapsulate and protect fields we use propeties!
 *              1.We use GET and SET accessors to implement propeties
 *              2. A Property with both GET and SET accessor is a READ/WRITE property
 *              3. A Property with only GET accessor is a READ ONLY property
 *              4. A property with only SET accessor is a WRITE ONLY property
 *              
 *         NOTE: - The advantage of properties over traditional Get() and Set() methods is that you can access them as if they were public fields!
 *         
 *         
 *         - Using properties we will protect class fields
 *         
 *         
 *         - If there is no additional logic in the property accessors, then we can make use of auto- implemented properties introduced in c# 3.0.
 *         - Auto implemented properties reduce amount of code that we have to write.
 *         - When you use auto-implemented properties, the compiler creates a private, anonymus field that can only be accessed through the property
 *           get and set accessors.
*/



/* We want this program to: 
 *   - Use propeties get set
 */



namespace _27._Properties
{
    public class Student
    {
        private int _id;           //to read these private fields we used get and set methods. Now we will do it with properties.
        private string _name;
        private int _passMark = 35;


        // AUTO implementation of properties:
        //private string _city;
        //private string _email;

        // if u have properties that doesnt include logic it is good to use autoimplement properities :
        public string City { get; set; }
            //get
            //{
            //    return this._city;
            //}
            //set
            //{
            //    this._city = value; 
            //}
        //}

        public string Email { get; set; }
        
            //get
            //{
            //    return this._email;
            //}
            //set
            //{
            //    this._email = value;
            //}
        //}




        // for ID read-write properties
        public int id 
        {

            set
            {
                if (value <= 0)  // value is build in keyword, this value is the one that recieves that we set to this property
                {
                    throw new Exception("Student Id should be greater than zero");
                }
                this._id = value;
            }


            get
            {
                return this._id;
            }

        }


        // for Name read-write properties
        public string Name
        {

            set
            {

                if (string.IsNullOrEmpty(value))
                {
                    throw new Exception("Name cannot be null or empty!");
                }
                this._name = value;

            }


            get
            {
                    if (string.IsNullOrEmpty(this._name))
                    {
                        return "No Name";
                    }
                    else
                    {
                        return this._name;
                    }
            }
        }


        // for PassMark - read-only properties
        public int GetPassMark
        
        {

            get 
            { 
            
            return this._passMark;

            }

        }

    }




    public class Program
    {
        // now we have complete control over fields, what gets and sets. Noone should be able to see the value.
        public static void Main()
        {
            Student C1 = new Student();
            C1.id=101;      // this 101 value gets passed into "value" keyword in set accessor
            Console.WriteLine($"Student Id = {C1.id}");  // c# will automatically understand that we want to pull this value get-return, so no need for get()method. 

            C1.Name ="TommyVerceti"; //try with null
            Console.WriteLine($"Student Name  = {C1.Name}");

        //    C1.GetPassMark = 30;  //can only read, cannot write.
            Console.WriteLine($"PassMark  = {C1.GetPassMark}");
        }
    }

}






//   *******************************************
//   Part 27  : Properties - END


