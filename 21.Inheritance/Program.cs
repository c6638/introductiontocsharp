﻿using System;



//   *******************************************
//    Part 21  : Inheritance  - START
/* 
 *            -  Why Inheritance  - 1.Example
 *            -  Advantages
 *            -  Inheritance Syntax
 *            -  Inheritance Concepts   
 *                       
 *           - Basically Inheritance allows us code reuse!
 *            
 *         1. Inheritance is on of the primary pillars of object oriented programming
 *         2. It Allows code reuse
 *         3. Code reuse can reduce time and errors
*/



/* We want this program to: 
 *  
 * 
 */

// 1. Example:
// A lot of code between these 2 classes is duplicated.

namespace _21.Inheritance
{

    //public class FullTimeEmployee
    //{
    //    string FirstName;
    //    string LastName;
    //    string Email;
    //    float YearlySalary;
    //    public void PrintFullName()
    //    {
    //    }
    //}

    //public class PartTimeEmployee
    //{
    //    string FirstName;
    //    string LastName;
    //    string Email;
    //    float HourlyRate;
    //    public void PrintFullName()
    //    {
    //    }
    //}



    // Move all the common code into our Base Employee class
    public class Employee
    {
        public string FirstName;
        public string LastName;
        public string Email;
        public void PrintFullName()
        {
            Console.WriteLine($"FistName = {FirstName} LastName = {LastName}");
        }

    }

    // syntax for inheritance
    // your "derived class : your base class"
    // whatever is presented in base class will also be avaliable to your derived class
    public class FullTymeEmployee: Employee
    {
        public float YearlySalary;
       
    }

    public class PartTimeEmployee : Employee
    {
        public float HourlyRate;

    }

    public class A : PartTimeEmployee  // A1 will have access to everything 
    {

    }

    public class Program
    {
        public static void Main()
        {
            FullTymeEmployee FTE = new FullTymeEmployee();
            FTE.FirstName = "Tommy";
            FTE.LastName = "Tech";
            FTE.YearlySalary = 50000;

            FTE.PrintFullName();

            PartTimeEmployee PTE = new PartTimeEmployee();
            PTE.FirstName = "Ann";
            PTE.LastName = "Lee";
            PTE.HourlyRate = 75000;
            
            PTE.PrintFullName();

            // Multi level inheritance is possible.
            A A1 = new A();  // A1 will have access to everything, thats avaliable a partimeEmployee, Employee
            A1.HourlyRate = 750;
        }
    }
}



//   *******************************************
//    Part 21  : Inheritance  - END