﻿using System;



//   *******************************************
//    Part 13  : While Looop  - START
/* 
 * 
 *      Loops:
 *          - While
 *          - Do, For, ForEach
 *          - Difference between while and do loop
 *      
 *      While Loop:
 *      1. While Loop checks the condition first.
 *      2. If condition is true, statements with in the loop are executed.
 *      3. This process is repeated as long as the condition evaluates to true.
 *      
 *      (NOTE: Don't forget to update the variable participating in the condition, so the loop can end at some point!)
 * 
*/



/* We want this program to: 
 *  
 *  - write program using while loop, which will ask the user to enter his target number.
 *  - then program should print even numbers until that number.
 *   example : enter 10
 *   0 2 4 6 8 10
 * 
 */


namespace _13.WhileLoop
{
    internal class Program
    {

        static void Main()
        {

            Console.WriteLine("Please enter your target number ");
            int userTarget = int.Parse(Console.ReadLine());

            int Start = 0;

            while (Start <= userTarget)
            {

                Console.WriteLine(Start);

                Start = Start + 2;

            }// increments by 2, and it repeats while condition is true!

        }
    }
}




//   *******************************************
//   Part 13 : While Loop  - END



