﻿using System;


//   *******************************************
//    Part 2 : Reading and Writing to console - START
/*              - Reading from the console
 *              - Writing to the console
 *              - 2 ways to write to console
 *                  a) Concatenation                                 > // Console.WriteLine("Hello " + userName);
 *                  b) Place holder syntax - most preferred          > // Console.WriteLine("Hello  {0},{1}", firstName,lastName);
 *                  c)* - try using ($"text{variable}"); - best      > // Console.WriteLine($"Hello {UserName}");
 *              - NOTE: that C# is case sensitive (keep it in mind)
 */



/* We want this program to print message to console with: 
 *  - prompt the user for his name
 *  - read the name from console
 *  - concatenate name with hello world, and print at the end.
 *  - use placeholder syntax
 
 */




namespace _2.ReadingWritingToConsole
{
    class Program
    {
        static void Main()
        {
            Console.WriteLine("Please enter your name: ");  // *to write something to the console we use console.writeline

        //  Console.ReadLine(); //*this function will read the line that user will write, as we can hover & see intellisence, we see it is a string.

            string UserName = Console.ReadLine();     //*string is Variable and UserName is name of that Variable.

            Console.WriteLine($"Hello {UserName}");


            Console.WriteLine("\nWelcome, now please enter your First Name; ");
            string FirstName = Console.ReadLine();


            Console.WriteLine("please also enter your Last Name; ");
            string LastName = Console.ReadLine();

            Console.WriteLine($"Welcome to training {FirstName} {LastName}");
        }
    }
}






//   *******************************************
//    Part 2 : Reading and Writing to console - END