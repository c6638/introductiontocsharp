﻿using System;

namespace _27.Properties__Adding_properties
{
    internal class Program
    {

        //1.Creating class
        // creating some of properties that car have
        class Car
        {
            //   public string Engine;  // 2.we name the propery = engine, we make it public member so that our object can access it,and string
            //   public string Engine;  - public member. we can read and write to member. But we want to create propety.


            // public string Engine { get; set; }  //3. now these are the properties.

            private string _engine;    //6.  whatever we pass into private property will be assigned to _engine
            public string Engine
            {

                get 
                {
                    return _engine;     //6.  whenever we try to read the value, it will return _engine
                }

                set 
                {
                    if(value =="V6" || value == "V8") //6.1. If the value that we are passing in is v8 or v6 it will be assigned, otherwise it will be empty.
                    {
                        _engine = value; //6.  whatever we pass into private property will be assigned to _engine
                    }
                    
                }
            }



            // public int Doors { get; set; }   //4.
            public  Car.Doors NumberOfDoors { get; set; } //5. Thats going to restrict, and we will have only 2 options to pick.
            public enum Doors
            {
                Two = 2,
                Four = 4
            }

            



            public string Color { get; set; }

        }








        static void Main()
        {
           
            Car myCar = new Car();
            myCar.Engine = "V56";

            // myCar.NumberOfDoors = 12;   //4. Technically this is correct, but logically car doesnt have 12 doors.

            myCar.NumberOfDoors = Car.Doors.Four; //5. We have two options, 2 or 4. Thats one way to restrict automatic properties.


            //myCar.Engine = "V56";              //6.2 It will be empty,
            myCar.Engine = "V8";
            Console.WriteLine( myCar.Engine );  //6.2   we are only looking for V6 or V8. It will print V8!




        }
    }
}
