﻿using System;


//   *******************************************
//                  Part 29  : Structs & Classes difference - START
/* 
 *                   - Structs are similar to classes. We use Struct keyword to create a struct.
 *                   - Structs can have fields, properties, constructors and methods - just like classes
 *                   - Several differences are between classes and structs
 *                   
 *                     - Struct is a value type where as class is a reference type.
 *                     - Structs are stored on stack, where as classes are stored on the heap
 *                     - Struct can't have destructors, but classes can have destructors
 *                     - Structs cannot have explicit parameter less constructo where as a class can
 *                     - Struct can't inherit from another class where as a class can. Both structs and classes can inherit from an interface.
 *                     
 *                     - 1. Value types are destroyed immediately after the scope is lost, where as for the reference types only the reference
 *                       variable is destroyed after the scope is lost.THe object is later destroyed by garbage collector.
 *                       
 *               STRUCT:
 *                      1.  Structs are value types, allocated either on the stack or inline in containing types.
 *                      2. Allocations and de-allocations of value types are in general cheaper than allocations and de-allocations of reference types.
 *                      3. 	In structs, each variable contains its own copy of the data (except in the case of the ref and out parameter variables), and an operation on one variable does not affect another variable.
 *                   
 *               CLASSES:
 *                      1. Classes are reference types, allocated on the heap and garbage-collected.
 *                      2. Assignments of large reference types are cheaper than assignments of large value types.
 *                      3. In classes, two variables can contain the reference of the same object and any operation on one variable can affect another variable.
 *                   
 *                   
*/


namespace _29.StructsAndClassesDifference
{

    internal class Program
    {

        public class Customer
        {
            public int ID { get; set; }
            public string Name { get; set; } 


        }


        static void Main()
        {    //1. Value types are destroyed immediately after the scope is lost..
            //simple program, how integer variable and object ID are created in memory:
            int i = 0;  // we can see int is a keyword for system struct(on hover)
            if (i == 10)
            {
                int j = 20;  // this j variable will be destroyed as soon as this block finishes executing.
                Customer C1 = new Customer();
                C1.ID = 101;    //but object reference will stay
                C1.Name = "Mark";
            }
            Console.WriteLine($"Hello");

        }
    }
}



//   *******************************************
//     Part 29  : Structs & Classes difference - END




