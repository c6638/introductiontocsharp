﻿using System;



//   *******************************************
//    Part 16  : Methods  - START
/*   
 *             - A Method is a block of code that performs a specific task. 
 *             - They are also called as functions.
 *             - They allow you to define a logic once, use it at many places
 *             
 *             
 *         - ReturnType - It specifies what type of value a method returns. 
 *         -  For example:
 *              - if a method has an int return type then it returns an int value.
 *              - if the method does not return a value, its return type is void.
 *       -  A C# method may or may not return a value. If the method doesn't return any value, we use the void keyword (shown in the above example).
 *          If the method returns any value, we use the return statement to return any value 
 *       
 *       
 *        [attributes]
 *        access-modifiers return-type  method-name (parameters)
 *        {
 *          Method-Body
 *        }
 *         
 *      > acces-modifiers : - public,private...
 *          > return-type : it can be any valid data type. If you dont want any return type then it can be void.
 *          > method-name : can be any name, except reserved words in c#    
 *          > parameters  : you can pass paramenters to your method,optional
 *          > body        : contains any valid c# statements & code
 *              
 *   Definition:         
 *              
 *              methodName - It is an identifier that is used to refer to the particular method in a program.
 *              method body - It includes the programming statements that are used to perform some tasks. 
 *                             The method body is enclosed inside the curly braces { }
 *                             
 *                             
 *                             
 *                             void display() 
 *                             {
 *                                 // code
 *                             }
 *                             
 *                       ->  Here, the name of the method is display(). And, the return type is void.
 *                             
 *                             
 *             -  In the above example, we have declared a method named display(). Now, to use the method, we need to call it.
 *              Here's how we can call the display() method.
 *              // calls the method
 *               display();   
 *               
 *               
 *               
 *               
 *               - The Difference between instance methods and static methods is that multiple instance of a calss can be created (or instatiated)
 *                  and each instance has its own seperate method. 
 *               -  However, when a method is static, there is not instance of that method,and you can invoke only that one definition of the staic method.
 *                  
 *                  
*/



/* We want this program to: 
 *  - I want to print even numbers
 */



namespace _16.Methods
{

     class Program
    {

        public static void Main()  // this method is called static method
        {
            // To invoke instance method, then we have to create instance of that class
            // instance - nothing but an object, with new keyword
            
            
            //1.
            //EvenNumbers();

            Program p = new Program();  // p is an object of a program class
            p.EvenNumbers();

            //if its public static void EvenNumbers(); then > Program.EvenNumbers();




            
            //2.
            //with parameters:
            p.EvenNumbers2(30);



        }
        // access modifier (public) - that method can be called from anywhere
        // return type - void we dont want to return anything
        // EvenNumbers  - name of function
        public  void EvenNumbers()    //this method is called instance method 
        {
            int Start = 0;

            while(Start <= 20)
            {
                Console.WriteLine($"Number :{Start}");
                Start = Start + 2;
            }
        }




       
        //Even numbers with passing parameters
        public void EvenNumbers2(int Target )    
        {
            int Start = 0;

            while (Start <= Target)
            {
                Console.WriteLine($"Number :{Start}");
                Start = Start + 2;
            }
        }


    }
}



//   *******************************************
//   Part 16 : Methods  - END



