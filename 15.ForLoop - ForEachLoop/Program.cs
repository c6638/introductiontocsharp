﻿using System;



//   *******************************************
//    Part 15  : For Looop  - START
/* 
 * 
 *      Loops:
 *          - While
 *          - Do, For, ForEach
 *          - Difference between while and do loop
 *      
 *      For Loop:
 *      1. For Loop is very similar to while loop. 
 *         - In a while loop we do the initialization at one place,
 *         - condition check at another place 
 *         - and modifying the variable at another place
 *         
 *        - Whereas For Loop has all of these at one place.
 *        
 *        
 *        ForEach Loop:
 *          - is basically used to iterate through items in a collection of something
 *          - for example ForEach loop can be used with arrays or collections such as ArrayList,HasTable, and Generics
 *       
 *       
 *       Break & Continue:
 *       
 *      - Break is usually used for exiting loops or used in switch statements
 *      - The break statement ends the loop immediately when it is encountered. Its syntax is: break;
 *      
 *      - When our program encounters the continue statement, the program control moves to the end of the loop and executes the test condition 
 *       (update statement in case of for loop). Its syntax is: continue;
 *      
 *      
 *      
*/



/* We want this program to: 
 *  -  Loop we will create integer array
 *  -  we will take elements from array and print them
 */



namespace _15.ForLoop___ForEachLoop
{

    internal class Program
    {

        static void Main()
        {

            int[] Numbers = new int[3];
            Numbers[0] = 101;
            Numbers[1] = 102;
            Numbers[2] = 103;


            //WHILE LOOP: -  initialization here
            int i = 0;
            while (i < Numbers.Length)  //condition check here
            {
                Console.WriteLine($"While Loop: {Numbers[i]}");//print the 1 number at 0 location  -priting location here
                i++;                               //increment number by 1 now its 2, and repeat
            }


            // FOR LOOP:  - initialization here

            for (int j = 0; j < Numbers.Length; j++)
            {
                Console.WriteLine($"For Loop : {Numbers[j]}");
            }

            
            // FOREACH LOOP :
            // for each loop will go and look at Numbers collection,
            // it takes the first element from this collection and puts it in k variable
            // it will loop as long as there are elements in collection
            foreach (int k in Numbers)
            {
                Console.WriteLine($"ForEach - loop : {k}");

            }


            BreakAndContinue1();
            BreakAndContinue2();
        }


 /* We want this program to: 
 *  - print numbers till 20
 *  - use break at 10 to end the loop
 */


        static void BreakAndContinue1() 
        {
            
            for(int i = 1; i <= 20; i++)
            {
                Console.WriteLine(i);
                if (i == 10)  // when i becomes 10 break statement executed, and exits loop,so rest of for loop will not be executed
                break;        // break get out of loop
            }

        }



/* We want this program to: 
*  - we want to print only even numbers, only until 20
* 
*/

        static void BreakAndContinue2()
        {

            for (int i = 0; i <= 20; i++)
            {
                if (i % 2 == 1)
                continue;          //when its met condition its not going to continue from here , it will skip rest of statmenet go back and loop again

                Console.WriteLine($"Continue : {i}");

            }

        }

    }
}




//   *******************************************
//   Part 15 : Loop  - END



