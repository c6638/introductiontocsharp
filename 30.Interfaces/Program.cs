﻿using System;
//   *******************************************
//                  Part 30  : Interfaces - START
/* 
 *                   - We create interfaces using interface keyword, just like classes
 *                     interfaces also contains properties, methods,delegates or events, but only declarations and no implementations
 *                     
 *                   - It is a compile time error to provide implementations for any interface member
 *                    
 *                   - Interface members are public by default, and they dont't allow explicit access modifiers.
 *                   
 *                   - Interfaces cannot contain fields.
 *                   
 *                   - If a class or a struct inherits from an interface, it mus provide implementation for all interface members
 *                     Otherwise we get a compile error.
 *                     
 *                   - A class or a struct can inherit from more than one interface at the same time, but where as a class cannot
 *                     inherit from more than one class at the same time.
 *                     
 *                   - Interfaces can inherit from other interfaces. A class that inherits this interface must provide
 *                     implementation for all interface members in the entire interface inheritance chain.
 *                     
 *                   - We cannot create an instance of an interface, but an interface reference variable can point to a derived class object.
 *                   
 *        
 *        
 */
namespace _30.Interfaces
{
    //To create interface we use interface keyword. ICustomer is common name convention

    interface ICustomer
    {
        void Print();  // we have declaration, but we cannot have implementation
                       // Console.WriteLine("Hello"); //--compiler error

        //Interface members are public by default!!
        //public void Print(); //we cannot do this, 

      //  int id; //we cannot do this, because interfaces cannot contain fields
    }

    // when a class inherits, we get compile error, class has to provide implementation of method
    class Customer : ICustomer
    {
        public void Print()
        {
            Console.WriteLine("Interface print method!");
        }
    }

    //classes provide multiple inheritance at same time
    interface I2
    {
        void I2Method();
    }

    class Program
    {
        static void Main()
        {
            Customer c1 = new Customer();
            c1.Print();
        }
    }
}



//   *******************************************
//     Part 30  : Interfaces - END