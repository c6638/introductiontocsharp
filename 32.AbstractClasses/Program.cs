﻿using System;


//                  *******************************************
//                      Part 32  : Abstract Classes  - START
/* 
 *                           - The Abstract keyword is used to create abstract class.
 *                           - An abstract class is incomplete and hence cannot be instantiated.
 *                           - An abstract class can only be used as base class.
 *                           - An abstract class canot be sealed.
 *                           - An abstract class may contain abstract members(methods,properties,indexers and events) but not mandatory.
 *                           - A non-abstract class derived from and abstract class must provide implementations for all inherited abstract members.
 *                           - Abstract class, and all of members of the class should have abstract as well
 *                           
 *                           If a class inherits an abstract class, there are 2 options available for that class :
 *                           1. Provide implementation for all the abstract members inherited from the base abstract class.
 *                           2. If the class does not wish to provide implementation for all the abstract members inherited from the abstract class,
 *                              then the class has to be marked as abstract.
 *                              
 *                          - Abstraction is nothing but hiding an implementation
*/



/* We want this program to: 
 *  
 *
 */


namespace _32.AbstractClasses
{

    public abstract class Customer  //1.1.created abstract class -> abstract classes are incomplete meaning they have abstract members. 
    {

        public abstract void Print();  //1.2. created abstract method - as member. with abstract keyword. It is there for any class that will derive it.
                                       // 

    }


  public class Program : Customer  //1.3. This program class can inherit from customer class. And we have to turn it to abstract class as well
    {                                       // if a class inherits from abstract class it has those 2 options.

        public override void Print()   //1.4. Build succeeds because of override:
        {
            Console.WriteLine("Print method");
        }



      public  static void Main()
        {

            Customer C = new Program();
            C.Print();
           
        }
    }
}




//   *******************************************
//   Part 32  : Abstract Classes  - END

