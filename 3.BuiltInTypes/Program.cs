﻿using System;

//   *******************************************
//    Part 3 : Built in types in C# - START
/*              - Boolean type - only true or false
 *              - Integral types - sbyte, byte, short, ushort, int,uint,long,ulong,char
 *              - Floating types  - float and double
 *              - String type
 *              
 *              
 *              https://www.youtube.com/watch?v=5g3TTPaGVzE&list=PLAC325451207E3105&index=4
 */



/* We want this program to: 
 
 */


namespace _3.BuiltInTypes
{
    class Program
    {
        static void Main()
        {

            bool B = true;  //*boolean datatype can hold only (true or false)

            //  B = 123;      // error

            int i = 0;       //* what is the minimum or maximum of this integer
            Console.WriteLine($"minimum = {int.MinValue} and \nmaximum = {int.MaxValue}");


            //they can hold whole number,if u want to store precision value.

            int il = 1234;
            Console.WriteLine(i);

            float f = 146.55f;
            Console.WriteLine(f);  // float can have fractional part

            double d = 1234.4634673473473437437d;
            Console.WriteLine(d);
        }
    }
}

/*
 * 
The type of a real literal is determined by its suffix as follows:

The literal without suffix or with the d or D suffix is of type double
The literal with the f or F suffix is of type float
The literal with the m or M suffix is of type decimal
The following code demonstrates an example of each:

double d = 3D;
d = 4d;
d = 3.934_001;

float f = 3_000.5F;
f = 5.4f;

decimal myMoney = 3_000.5m;
myMoney = 400.75M;
 
 
*/

//   *******************************************
//    Part 3 : Built in types in C# - END