﻿using System;


//   *******************************************
//    Part   : Indexers  - START
/*            
 *            - Indexers allow instance of a class or struct to be indexed just like arrays.
 *            - The indexed value can be set or retrieved without explicitly specifiying a type or instance member.
 *            - Indexers resemble properties except that their accessors take parameters.
 *            
 *                  Indexers enable objects to be indexed in a similar manner to arrays.
 *            
 *                  A get accessor returns a value. A set accessor assigns a value.
 *                   
 *                  The this keyword is used to define the indexer.
 *           
 *                  The this keyword is used to define the indexer.
 *            
 *                  The value keyword is used to define the value being assigned by the set accessor.
 *            
 *                  Indexers do not have to be indexed by an integer value; it is up to you how to define the specific look-up mechanism.   
 *           
 *                  Indexers can be overloaded.
 *           
 *                  The value keyword is used to define the value being assigned by the set accessor.
 *                   
 *                  Indexers do not have to be indexed by an integer value; it is up to you how to define the specific look-up mechanism.
 *
 *            
*/



/* We want this program to: 
 * The following example defines a generic class with simple get and set accessor methods to assign and retrieve values.
 * The Program class creates an instance of this class for storing strings.
 */
// The example displays the following output:
//       Hello, World.


//1.example:
//
namespace Indexers
{

    class SampleCollection<T>
    {

        //declare an array to store data elements

        private T[] arr = new T[100];

       //define indexer to allow client code to use [] notation.
       public T this[int i]
        {
            get { return arr[i]; }
            set { arr[i] = value; }
        }
    } 

    class Program
    {
        static void Main()
        {
            var stringCollection = new SampleCollection<string>();
            stringCollection[0] = "Hello, World!";
            stringCollection[1] = "Hello You!";
            Console.WriteLine(stringCollection[0]);
            Console.WriteLine(stringCollection[1]);
        }
    }
}
//


//2.Example:
/*
namespace Indexers
{

    class SampleCollection<T>
    {

        //declare an array to store data elements

        private T[] arr = new T[100];

        //define indexer to allow client code to use [] notation.
        public T this[int i]
        {
            get => arr[i];
            set => arr[i] = value;
        }
    }

    class Program
    {
        static void Main()
        {
            var stringCollection = new SampleCollection<string>();
            stringCollection[0] = "Hello, World!";
            Console.WriteLine(stringCollection[0]);
        }
    }
}

*/

//   *******************************************
//   Part  : Indexers - END


