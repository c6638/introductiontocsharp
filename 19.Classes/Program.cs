﻿using System;



//   *******************************************
//    Part 19  : Classes  - START
/* 
 *            - Class consists of data and behaviour.
 *              Class data is represented by it's fields and behaviour is represented by its methods.
 *              
 *           - We use the class keyword to create an object. 
 *           - Class can contain: 
 *              fields - variables to store data
 *              methods - functions to perform specific tasks
 *              
 *          - Purpose of a class constructor is to initialize class fields.
 *          - Construtctor doesnt return values, and has the same name as the class.
 *           
*/



/* We want this program to: 
 *  
 * 
 */


namespace _19.Classes
{

    class Customer
    {
        //these 2 strings represent the data that this class is having
        string _firstName;
        string _lastName;


        // to initialize these fields the class can also have constructor
        // consturctor is mainly used to initialize your class fields
        // constructor has the same name as the class, but constructor doesnt return the value,but it can take parameters

        public Customer(string firstName, string lastName)
        {
            this._firstName = firstName;
            this._lastName = lastName;
        }
        //this keyword "this." actually refers to an instance of the class, to object of this class
        //this constructor is getting automatically called when we are creating instance of customer class

        public void PrintFullName()
        {
            Console.WriteLine($"Full Name = {this._firstName} {this._lastName}");
        }

        //Destructor -  usually for cleaning up any resources class was holding. They are automatically called. 
        //It has tilled simbol and its same name as your class.
        ~Customer()
        {
            //clean up code
        }
    }




     class Program
    {

        public static void Main()
        {
            //now we use our created class and we create instance of our class

            Customer C1 = new Customer("Chris", "K");
            C1.PrintFullName();


            // after we created a object, we print full name
            // constructor is ensuring that fields are populated before we use it in method.

        }

    }


}




//   *******************************************
//    Part 19  : Classes  - END



