﻿using System;


//   *******************************************
//    Part 17  : Method Parameter Types  - START
/* 
 *       - The 4 types of method parameters
 *       - Value Parameters
 *       - Reference Parameters
 *       - Out Paramenters
 *       - Parameters Arrays
 *       
 *       - Method parameters Vs Method Arguments
 *       
 *       - whenever we declare a method (these are called parameters), but when we invoke method we pass values and they are called arguments method(arguments).
 *       - A parameter is a variable in a method definition.
 *       - Parameters refers to the list of variables in a method declaration
 *       
*/



/* We want this program to: 
 *  
 *  
 */

namespace _17.MethodParameterTypes
{

    internal class Program
    {
        //Passing the parameter as value
       public static void Main()
        {
        /*
            //1.PASS BY VALUE
            // we pass i as parameter of this method.
            int i = 0;
           SimpleMethod(i); 
            Console.WriteLine(i);  //initial variable i will be 0
        */

            //2.PASS BY REFERENCE
            //since we are passing i as a reference, this value 
            int a = 0;
            SimpleMethod(ref a);
            Console.WriteLine(a);

            //since we are passing i as a reference, this value 


            //3.OUT keyword - invoking the method and using out keyword
            int total = 0;
            int product = 0;
            Calculate(2, 5, out total, out product);
            Console.WriteLine($"Sum = {total} \nProduct = {product}");



            //4. PARAMETER ARRAYS -
            int[] Numbers = new int[3];
            Numbers[0] = 101;
            Numbers[1] = 102;
            Numbers[2] = 103;

            //ParamasMethod();  
            //ParamsMethod(1,2,3,4,5);
            ParamsMethod(Numbers);

        }


        // whatever value you pass into this method it will change the value of existing one that is 101
    
        /* 1.public static void SimpleMethod(int j)   */
        //2.
       public static void SimpleMethod(ref int j)  //using ref -> changes it to reference parameter
        {

        j = 101;

        }


        //3. when you create output parameters use the out keyword not only in declaration but in place where u invoke the method and you have to use the out keyword
        public static void Calculate(int F, int N, out int Sum, out int Product)
        {
            Sum = F + N;
            Product = F * N;
        }


        //4.Parameter Arrays
        public static void ParamsMethod(params int[] Numbers) // when we want to make our parameters optional, to either put or have existing array, use "params int"
        {
            Console.WriteLine($"There are {Numbers.Length} elements");

            foreach(int i in Numbers)
            {
                Console.WriteLine(i);
            }
        }

    }
}






//   *******************************************
//   Part 17  : Method Parameter Types  - END
