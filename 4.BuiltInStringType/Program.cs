﻿using System;


//   *******************************************
//    Part 4 : Built in string type in C# - START
/*             - \  (backslash) is used as escape sequence sequence character
*              - @  (when you put @ infront of string, all escape sequences are no longer treated like that, instead as 'regular' printable character)
*              -  Verbatim literal is a string with an @ symbol prefix, as in @"Hello".Makes escape sequences translate to normal printable characters.
*              - \n  new line     
*           
*/



/* We want this program to: 
 
 */

namespace _4.BuiltInStringType
{
    class Program
    {
        static void Main()
        {
            
            string Name = "\"Pragim\"";   // use backslash to print quotes
            string lastName = "\nĐon";   // use \n for new line
            string abName = "C:\\Pragim\\ProgramFiles\\Training";   // less readable 
            string bName = @"C:\Pragim\ProgramFiles\Training";   // verbatim literal - better readable
            Console.WriteLine($"{Name} and{lastName} \nLessReadable: {abName}\nBetter readable: {bName}");

        }
    }
}




//   *******************************************
//    Part 4 : Built in string type in C# - END