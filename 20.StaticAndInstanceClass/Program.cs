﻿using System;


//   *******************************************
//    Part 20  : Static And Instance Class Members  - START
/* 
 *            - Static class members
 *            - Instance class members
 *            - Difference between static and instance members
 *            - and example why you should make certain members static
 *            
 *            
 *            - Static members: are invoked using the name of the class
 *            - Instance members: are invoked using an instance or object of class
 *            
 *            -Static member doesn't change:
 *            
 *            static float PI;
 *            int Radius;
 *            
 *            static circle()
 *            {
 *            Circle.PI = 3.14f;
 *            }
 *           
*/



/* We want this program to: 
 *  
 * 
 */


namespace _20.StaticAndInstanceClass
{
    //1. Example:
    /*
        //definition of static and instance class
        class Circle
        {
            static float _pi = 3.14f;  // trailing - if used then in calling should be name of class

            int _radius;




            //using constructor - to initialize class fields
            public Circle(int Radius)
            {
                this._radius = Radius;                         
            }

            public static void Printing()  // static method to print something
            {
                Console.WriteLine($" ");
            }



            //creating method to calculate area of the circle and return it back

            public float CalculateArea()
            {

                return Circle._pi * this._radius * this._radius;  //if used static should be name of class circle
            }

        }




        internal class Program
        {
            static void Main()
            {

                //we created here 2 circle objects Area1,Area2

                Circle C1 = new Circle(5);
                float Area1 = C1.CalculateArea();
                Console.WriteLine($"Area = : {Area1}");


                Circle C2 = new Circle(6);
                float Area2 = C2.CalculateArea();
                Console.WriteLine($"Area = {Area2}");

            }
        }
    }


    */
    /*
        //2. Example
        //If a variable is declared static, we can access the variable using the class name. For example,

        class Student
        {

            //static variable
            public static string department = "Computer science";

        }

        class Program
        {

            static void Main()
            {

                // access static variable
                Console.WriteLine("Department: " + Student.department);

                Console.ReadLine();
            }
        }
    }

    */

    /*
        //3. Example
        // In C#, every object of a class will have its own copy of instance variables. For example,
        class Student
        {
            // instance variable
            public string studentName;
        }

        class Program
        {
            static void Main()
            {

                Student s1 = new Student();
                Student s2 = new Student();

            }
        }
    }
    //Here, both the objects s1 and s2 will have separate copies of the variable studentName. And, they are different from each other.
    // However, if we declare a variable static, all objects of the class share the same static variable.
    // And, we don't need to create objects of the class to access the static variables.



    */
    //
    // 3. Example


    class Student
    {
        static public string schoolName = "Programiz School";
        public string studentName;

    }

    class Program
    {
        static void Main()
        {

            Student s1 = new Student();
            s1.studentName = "Ram";

            // calls instance variable
            Console.WriteLine("Name: " + s1.studentName);
            // calls static variable
            Console.WriteLine("School: " + Student.schoolName);

            Student s2 = new Student();
            s2.studentName = "Shyam";

            // calls instance variable
            Console.WriteLine("Name: " + s2.studentName);
            // calls static variable
            Console.WriteLine("School: " + Student.schoolName);


            Console.ReadLine();
        }
    }
}


/*
 * In the above program, the Student class has a non-static variable named studentName and a static variable named schoolName.
Inside the Program class, s1.studentName / s2.studentName - calls the non-static variable using objects s1 and s2 respectively
Student.schoolName - calls the static variable by using the class name
Since the schoolName is the same for all students, it is good to make the schoolName static. It saves memory and makes the program more efficient.
*/



//   *******************************************
//   Part 20  : Static And Instance Class Members - END


