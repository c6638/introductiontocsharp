﻿using System;




//   *******************************************
//                  Part 28  : Structs  - START
/* 
 *                   - Just like classes structs can have:
 *                                  1.Private fields
 *                                  2. Public properties
 *                                  3. Constructors
 *                                  4. Methods
 *                  - Object initializer syntax can be used to initialize either a struct or a class
 *                  - There are several differences between classes and structs which we will be looking later on
 *                  
*/



namespace _28.Structs
{
    // creating struct customer
    public struct Customer
    {
        private int _id;
        private string _name;
         

        //now we create properties for this field: to set and get value
        public int ID
        {
            get { return this._id; }
            set { this._id = value; }
        }

        //this property encapsulates this field
        public string Name
        {
            get { return this._name; }  
            set { this._name = value; } 
        }


        //now constructor to initialize struct fields
        public Customer(int Id, string Name)
        {
            this._id = Id;
            this._name = Name;  
        }

        public void PrintDetails()
        {
            Console.WriteLine($"ID = {this._id} \nName = {this._name}");
        }

    }

    internal class Program
    {

        static void Main()
        {
            //now we create an instance of a structure
            Customer C1 = new Customer(105, "Mark");
            C1.PrintDetails();

            //if we print like this we havent initialized customer fields
            Customer C2 = new Customer();
            C1.ID = 102;
            C2.Name = "John";
            C2.PrintDetails();

            //using Struct to initialize:
            Customer C3 = new Customer
            {
                ID = 107,
                Name = "Mary"
            };
            C3.PrintDetails();


        }
    }
}



//   *******************************************
//   Part 28  : Structs - END


