﻿using System;



//   *******************************************
//    Part 26  : Why  Properties  - START
/*           
 *           - Marking the class fields public and exposing them to the external world is bad,
 *              as you will not have control over what gets assigned and returned
 *            
 *            
 *           - Programmming languages that does not have properties use getter and setter methods to encapsulate and protect fields
 *           - Encapsulation is one of the primary pillars of object oriented programming
 *           - Get & Set Methods
 *           
 *     Note: - Never expose your class fields publicly. Encapsulate them to make it safer.
*/




/* We want this program to: 
 * 
 * 
 */

namespace _26.WhyProperties
{

    /* 1. Example - START:
        internal class Program
        {
    //      - Marking the class fields public and exposing them to the external world is bad,
                as you will not have control over what gets assigned and returned

    
              Problems with Public Fields:
                1. ID should always be non negative number.
                2. Name cannot be set to NULL
                3. If Student Name is missing "No Name" should be returned
                4. PassMark should be read only

                Check below:

     // 

            public class Student
            {
                public int ID;
                public string Name;
                public int PassMark = 35;

            }

            public static void Main()
            {
                Student C1 = new Student();
                C1.ID = -101;           //1.here we are able to do it, negative id - passmark, null missing name...
                C1.Name = null;           //   2. Name cannot be set to NULL
                C1.PassMark = 0;          
                Console.WriteLine($"ID = {C1.ID} Name = {C1.Name}, PassMark = {C1.PassMark}");

            } //in this example all the bussiness violations are happening here
              // and we want to control what gets out of these fields and what gets into. (we can make private but thats not it)
        }

      1. Example -  END:
    */


    // 2. Example - START
    //          In this example we use SetId(int id) and GetId() methods to encapsulate _id class field
    //          As result we have better control on what gets assigned and returned from the _id field!
    //           - **protecting class fields with public methods get & set **

    public class Student
    {

        // with private we are not exposing these fields to external world. Get & Set methods are protecting this private field.
        private int _id;  // underscore if its private _id;  (naming convention)
        private string _name;
        private int _passMark = 35;

        // and now somebody should be able to set id value and read id value
        // for ID
        public void SetId(int id)  //this just sets the value as a parameter
        {
            if (id <= 0)
            {
                throw new Exception("Student Id should be greater than zero");//exception is nothing but an error condition
            }
            this._id = id;
        }


        public int GetId() // it we want to read the value - get method. Will simply return id value.
        {
            return this._id;
        }



        // for Name
        public void SetName(string Name)
        {
            if (string.IsNullOrEmpty(Name))
            {
                throw new Exception("Name cannot be null or empty!");
            }
            this._name = Name;
        }

        public string GetName()
        {
            if (string.IsNullOrEmpty(this._name))
            {
                return "No Name";
            }
            else
            {
                return this._name;
            }
            
        }

        // for PassMark
        public int GetPassMark()
        {
            //nobody will be able to change the value, but wants to know will be able to get passmark method.
            return this._passMark;
        }

    }




    public class Program
    {
        // now we have complete control over fields, what gets and sets. Noone should be able to see the value.
        public static void Main()
        {
            Student C1 = new Student();
            C1.SetId(101);   // if its negative it will get an exception error
            //when we write c1. we dont see any fields(id),we only see set & get id

            Console.WriteLine($"Student Id = {C1.GetId()}"); // getid is simply going to return the value that is stored in private field
           
            C1.SetName("TommyVerceti"); //try with null
            Console.WriteLine($"Student Name  = {C1.GetName()}");

            
            Console.WriteLine($"PassMark  = {C1.GetPassMark()}");
        }
    }

}

// 2. Example - END







//   *******************************************
//   Part 26  : Why Properties - END





