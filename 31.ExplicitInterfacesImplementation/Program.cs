﻿using System;

using System;
//   *******************************************
//                  Part 31  : Explicit Interfaces implementation - START
/* 
 *                   - Explicit interface implementation
 *                   - Interfaces cannot have implemenetation, only declarations
 *    
 * 
 * 
 *                    1. When you are explicitly implementing an interface member you are not allowed to use access modifier
 *                    2. You have to use interface name within that interface which you are implementing so
 *                              interface name then . and then the method name from interface you are implementing
 *                           
 *                                               /public/ void I1.InterfaceMethd()  
 *                                                 //we are telling explicitly from which interface is this comming from
 *                    
 */



namespace _31.ExplicitInterfacesImplementation
{
    //interface with member interfacemethd 
    //interfaces cannot have implementation only declaration:
    interface I1
    {
        void InterfaceMethd();

    }
    //we add another Interface:
    interface I2
    {
        void InterfaceMethd();

    }




    //If we implement interface I1, class has to provide implemenetation for that, method!
    class Program : I1, I2 // this class now inherit from I1 and I2
    {
        //to make it explicit without access modifier no public

        /*public*/ void I1.InterfaceMethd()  // we dont know which interface we are calling so it is good to use explicit interface implemenetation
        {

            Console.WriteLine("I1 Interface method!");
        }

        
        void I2.InterfaceMethd()  // we dont know which interface we are calling so it is good to use explicit interface implemenetation
                                  //we are telling explicitly from which interface is this comming from
        {

            Console.WriteLine("I2 Interface method!");
        }



        static void Main()
        {

            //lets invoke method by creating instance of the class:
            Program P1 = new Program();
            //P1.InterfaceMethd();     // when we call explicitly interface you will get compile error

            // lets invoke some of interface reference variable:
            ((I1)P1).InterfaceMethd();
            ((I2)P1).InterfaceMethd();

        }
    }
}


//   *******************************************
//     Part 31  : Explicit Interfaces implementation - END