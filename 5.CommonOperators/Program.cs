﻿using System;


//   *******************************************
//    Part 5 : Common Operators in C# - START
/*             - Assignement Operator  =
 *             - Arithmetic Operators like +, -, *, /, %
 *             - Comparison Operators like ==, !=, >, >=, <, <=
 *             - Conditional Operators like &&, ||
 *             - Ternary Operator ?:
 *             - Null Coalescing Operator ??
*/



/* We want this program to: 
 
 */


namespace _5.CommonOperators
{
    class Program
    {
        static void Main()
        {



            int Numerator = 10;
            int Denominator = 2;

            int Result = Numerator / Denominator;
            Console.WriteLine($"Result = {Result}");




            //if we have to check if the number is 50
            //  then we will have to use == 
            int Number = 100;
            if (Number == 100)
            {
                Console.WriteLine("number is 100");
            }
            else
            {
                Console.WriteLine("number is not 100");
            }


            // in this case both the conditions should be satisfied:  &&
            int NumberA = 10;
            int NumberB = 20;

            if (NumberA == 10 && NumberB == 20)
            {
                Console.WriteLine("Numbers are 10 and 20, correct!");
            }
            else
            {
                Console.WriteLine("Numbers are NOT correct!");
            }



            // in this case we use OR operator:  || 
            // if any of these conditions are satisfied this code will be executed
            int NumberC = 3;
            int NumberD = 2;

            if (NumberC == 3 || NumberD == 2)
            {
                Console.WriteLine("\nNumberC,NumberD are Correct - one of these conditions satisfied!");
            }



            // in this case we use bool :  true / false 
            int myNumber = 100;
            bool isNumber;

            if (myNumber == 100)
            {
                isNumber = true;
            }
            else
            {
                isNumber = false;
            }

            Console.WriteLine($"\nMy Number 100 is {isNumber}");



            // all of these above can be written in single line of code
            //Ternary operator:

            int NumberX = 15;

            bool isNumberX = NumberX == 15 ? true : false;

            Console.WriteLine($"\nMy Number 15 is  {isNumberX}");


        }
    }
}




//   *******************************************
//   Part 5 : Common Operators in C# - END


