﻿using System;

//   *******************************************
//    Part 9  : Comments  - START
/* 
 *       - Single line comments         //
 *       - Multi line comments          / *   * /
 *       - XML documentation comments   ///
 *       
 *       
 *       - Comments are used to document what the program does and what specific blocks or lines of code do.
 *       - C# compiler ignores comments.
 *       
 *       To comment and uncomment, there are 2 ways
 *       1.Use the designer
 *       2.Keyboard Shortcut: ctrl+k,ctrl+c and ctrl+k, ctrl+u
 *       
 *       Note: Don't try to comment every line of code, use comments only for blocks or lines of code that are difficult to understand!
 *       
*/



/* We want this program to: 
 * 
 */


namespace _9.Comments
{


    internal class Program
    {

        /// <summary>
        /// This is a sample class and a sample documentation comment.
        /// </summary>
        static void Main()  // now summary is shown in intellisence
        {

            Console.WriteLine("Hello");
            Console.WriteLine("Hello");

          
        }
    }
}









//   *******************************************
//   Part 8 : Comments - END


