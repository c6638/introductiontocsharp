﻿using System;




//   *******************************************
//    Part 7 : Data Type Conversions  - START
/*            
 *              Implicit Conversions
 *                      Implicit conversion is done by the compiler:
 *                        1. When there is no loss of information if the conversion is done
 *                        2. If there is no possibility of throwing exceptions during the conversion
 *                        
 *                      Example:
 *                       Converting an "int" to "float" will not loose any data and no exception will be thrown,
 *                       hence an implicit conversion can be done.
 *                       
 *                      Example:
 *                      Convertin a float into int - we loose the fractional part and this case an explicit 
 *                      conversion is required.
 *                      
 *                      
 *              Explicit Conversions
 *              
 *              
 *              
 *              Difference between Parse() and TryParse
 *                  If the number is in a string format you have 2 options :
 *                          
 *                          Parse ()
 *                          
 *                          TryParse()
 *                          
 *                  Parse() method throws an exception if it cannot parse the value, whereas
 *                  TryParse() returns a bool indicating whether it succeeded or failed
 *                  
 *                  Use Parse() if you are sure the value will be valid, otherwise use TryParse()
 *             
*/



/* We want this program to: 
     Implicit Conversion
      - from int to float
      - float to int
     
 */

namespace _7.DatatypeConversions
{
    internal class Program
    {
        //Converting integer into float datatype:
        static void ImplicitConversion1()
        {
            int i = 100;

            float f = i;

            Console.WriteLine(f);

        }

        static void ExplicitConversion()
        {
            float f = 123.456F;  // with sufix F at the end

            // Cannot implicitly convert float to int
            // Fractional part will be lost.
            // Float is a bigger data type than int, so there is also a possibility fo overflow exception

       //     int i =  f;  // we cannot be implicitly converted to int because its bigger datatype.

            int j = (int)f;  //Explicit conversion using  cast operator

            int k = Convert.ToInt32(f);  // Explicit conversion using convert class, anydatatype to anydatatype

            Console.WriteLine(j);   

        }


        // Parse()  &  TryParse () conversion
        // converting one datatype to another 
        static void Parsing()
        {
            string strNumberA = "100";
            int iA = int.Parse(strNumberA); 

            Console.WriteLine($"Result of Parse method : {iA}");


            string strNumberB = "100ABC";
            int result = 0;

            int.TryParse(strNumberB, out result); //its going to take string, convert it into integer and store it into variable  (out)

            Console.WriteLine($"Result of TryParse method :{result}");

        }



        // Is stringa u number:


        static void Parse2()
        {
            int intA;

            string stringA = "5";

            intA = int.Parse(stringA);
            Console.WriteLine($"The result of parsing string to number : {stringA}");

        }




            static void Main()
        {
            ImplicitConversion1();
            ExplicitConversion();
            Parsing();
            Parse2();
        }





    }
}



//   *******************************************
//   Part 7 : Data Type Conversions - END