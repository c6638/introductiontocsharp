﻿using System;



//   *******************************************
//    Part 14  : Do While Looop  - START
/* 
 * 
 *      Loops:
 *          - While
 *          - Do, For, ForEach
 *          - Difference between while and do loop
 *      
 *      Do While Loop:
 *      1. Do While Loop Checks it's condition at the end of the loop
 *      2. This means that the "Do Loop" is guaranteed to execute at least one time
 *      3. Usually Do Loops are used to present a menu to the user
 * 
*/



/* We want this program to: 
 *  -  user enters number
 *  -  we want the user to stop printing the even numbers
 */

namespace _14.DoWhileLoop
{

    internal class Program
    {

        static void Main()

        {
            string userChoice = string.Empty;
            do
            {

                Console.WriteLine("Please enter you target number");
                int userTarget = int.Parse(Console.ReadLine());

                int Start = 0;


                while (Start <= userTarget)
                {

                    Console.WriteLine(Start + "  ");

                    Start = Start + 2;
                }

                do
                {
                    Console.WriteLine("Do you want to Contine - yes or no");

                    userChoice = Console.ReadLine().ToLower();

                    if (userChoice != "yes" && userChoice != "no")
                    {
                        Console.WriteLine("Invalid input, u did not write yes or no, please write: YES or YES");

                    }//  we want keep asking user for input yes or no, untill he writes yes or no. And we need a loop for that


                } while (userChoice != "yes" && userChoice != "no");


            } while (userChoice == "yes");
        }
    }
}

//   *******************************************
//   Part 14 : Do While Loop  - END

