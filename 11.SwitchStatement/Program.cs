﻿using System;


//   *******************************************
//    Part 11  : Switch Statement  - START
/* 
 *       - switch statement
 *       - break statement - is used inside a switch statement the controll will leave the switch statement
 *       - goto statement  - you can either jump to another case statement or to a specific label  ((it is a bad practice to use goto))
 *     
 *     - Multiple if statements can be replaced with a switch statement
*/



/* We want this program to: 
 * 
 */


namespace _11.SwitchStatement
{


    internal class Program
    {
        static void Main()

        {
            withIf();
            withSwitch();

        }


        static void withIf()
        {
            Console.WriteLine("Please enter a number");
            int userNumber = int.Parse(Console.ReadLine());

            if (userNumber == 10)
            {
                Console.WriteLine("Your number is 10");
            }
            else if (userNumber == 20)
            {
                Console.WriteLine("Your number is 20");
            }
            else if (userNumber == 30)
            {
                Console.WriteLine("Your number is 30");
            }
            else
            {
                Console.WriteLine("Your number is not 10,20 or 30!");
            }

        }



            static void withSwitch()
            {

                Console.WriteLine("Please enter a number");
                int userNumber = int.Parse(Console.ReadLine());

                switch (userNumber)
                {

                    case 10:
                        Console.WriteLine("Your number is 10");
                        break;

                    case 20:
                        Console.WriteLine("Your number is 20");
                        break;
                    case 30:
                        Console.WriteLine("Your number is 30");
                        break;


                    default:
                        Console.WriteLine("Your number is not 10,20 or 30");
                        break;


                }

            }
        }
    }








//   *******************************************
//   Part 11 : Switch Statement - END



